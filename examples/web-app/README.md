# Web App CI Role

This is a minimal example of a role definition that will allow the creation of
resource groups, App Service Plan, Web App and Slots. The subscription id is
parameterised so that you can apply the role definition to your own Azure
subscription immediately.

## Usage

Set the environment variable subscription_id to the id of the subscription that
is currently default in the Azure CLI.

```bash
$ export subscription_id=$(az account show --query id --output tsv)
```

Generate the role definition json

```bash
$ jsonnet \
  --jpath ../.. \
  --tla-str subscription_id \
  --output-file web-app-ci-role.json \
  web-app-ci-role.jsonnet
```

Create a unique id for the role definition

```bash
$ export role_id=$(uuidgen)
```

Create the role definition

```bash
$ az rest \
  --method PUT \
  --uri "https://management.azure.com/subscriptions/$subscription_id/providers/Microsoft.Authorization/roleDefinitions/$role_id?api-version=2018-01-01-preview" \
  --body @web-app-ci-role.json
```

If you then want to change the role definition, make the changes to the
`web-app-ci-role.jsonnet`, for example removing the `app_service_slot` resource.

Re-generate the json

```bash
$ jsonnet \
  --jpath ../.. \
  --tla-str subscription_id \
  --output-file web-app-ci-role.json \
  web-app-ci-role.jsonnet
```

And update the role definition

```bash
$ az rest \
  --method PUT \
  --uri "https://management.azure.com/subscriptions/$subscription_id/providers/Microsoft.Authorization/roleDefinitions/$role_id?api-version=2018-01-01-preview" \
  --body @web-app-ci-role.json
```
