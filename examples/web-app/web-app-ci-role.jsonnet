local role = import 'tf-role-definition.libsonnet';
local res = import 'tf-resource-actions.libsonnet';

function(subscription_id)
  role.new(
    "slim-ci-role",
    "Allow only the actions necesary for running Terraform",
    [
      res.resource_group,
      res.app_service_plan,
      res.app_service,
      res.app_service_slot,
    ],
    ['/subscriptions/' + subscription_id],
    subscription_id,
  )
