{
  resource_group: {
    Actions: [
      "Microsoft.Resources/subscriptions/resourceGroups/read",
      "Microsoft.Resources/subscriptions/resourceGroups/write",
    ],
    DataActions: [],
  },
  app_service_plan: {
    Actions: [
      "microsoft.web/register/action",
      "Microsoft.Web/serverfarms/Read",
      "Microsoft.Web/serverfarms/Write",
    ],
    DataActions: [],
  },
  app_service: {
    Actions: [
      "Microsoft.Web/sites/read",
      "Microsoft.Web/sites/write",
      "Microsoft.Web/sites/config/read",
      "Microsoft.Web/sites/config/write",
      "Microsoft.Web/sites/config/list/action",
    ],
    DataActions: [],
  },
  app_service_slot: {
    Actions: [
      "Microsoft.Web/sites/slots/write",
      "Microsoft.Web/sites/slots/delete",
      "Microsoft.Web/sites/slots/config/write",
      "Microsoft.Web/sites/slots/config/list/action",
    ],
    DataActions: [],
  },
  monitor_action_group: {
    Actions: [
      "microsoft.insights/actionGroups/read",
      "microsoft.insights/actionGroups/write",
    ],
    DataActions: [],
  },
  consumption_budget_subscription: {
    Actions: [
      // https://docs.microsoft.com/en-us/answers/questions/400083/what-are-the-minimum-permissions-required-to-creat.html
      "*/read",
      "Microsoft.Consumption/budgets/read",
      "Microsoft.Consumption/budgets/write",
    ],
    DataActions: [],
  },
}
