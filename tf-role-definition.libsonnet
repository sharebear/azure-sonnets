{
  new(name, description, resources, assignable_scopes, subscription_id)::
  {
    properties: {
      roleName: name,
      type: "CustomRole",
      description: description,
      permissions: [{
        actions: std.uniq(std.sort(std.flattenArrays([x.Actions for x in resources]))),
        notActions: [],
        dataActions: std.uniq(std.sort(std.flattenArrays([x.DataActions for x in resources]))),
        notDataActions: [],
      }],
      assignableScopes: assignable_scopes,
    },
  },
}
