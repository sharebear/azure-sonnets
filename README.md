# Azure Sonnets

> :zap: This project currently has Proof of Concept status and isn't actively
> maintained. This could change if someone shows an interest.

Collection of jsonnet libraries that can be useful when working with Microsoft
Azure. The general idea is that you can use these to generate a minimal role
definition to give Terraform the absolute minimum access necessary to do it's
job.

## tf-resource-actions.libsonnet

jsonnet library that contains a mapping of Terraform Azure Provider resources
and the minimal Azure RBAC actions that are necessary for Terraform to create
and update the resources.

## tf-role-definition.libsonnet

jsonnet library containing a function that generates well formed input to the
[Role Definitions - Create Or Update][rest] REST API. This takes as input a list
of the mappings from `tf-resource-actions.libsonnet`.

Note: I couldn't find REST API docs for version 2018-01-01-preview of the API,
but found the [ARM Template docs][arm] do have a definition, that I relied on
for information on `dataActions`.

## Usage

Take a look at the [example](examples/web-app) for step by step instructions for
creating a role definition.

## Future work

1. Consider making mappings in `tf-resource-actions.libsonnet` more granular
   (split into read/createorupdate/delete)
2. Extend the list of mappings for more resources
3. Create CLI tool that can parse terraform files to get the list of resources
   used, run jsonnet using that list as input and use the
   [Azure SDK][azure-sdk-for-go] to create or update the role definition
   seamlessly.

[rest]: https://docs.microsoft.com/en-us/rest/api/authorization/role-definitions/create-or-update
[arm]: https://docs.microsoft.com/en-us/azure/templates/microsoft.authorization/2018-01-01-preview/roledefinitions?tabs=json
[azure-sdk-for-go]: https://github.com/Azure/azure-sdk-for-go
